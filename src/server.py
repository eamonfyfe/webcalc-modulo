from flask import request, jsonify
from flask import Flask
import modulo

app = Flask(__name__)

host = '0.0.0.0'
port = 80

output = {
    'error': False,
    'string': '',
    'answer': 0
}

resp = request.get(url='/', params=output)
resp.headers.setdefault('Content-Type', 'application/json')
resp.headers.setdefault('Access-Control-Allow-Origin', '*')


@app.route('/')
def values():
    x = request.args.get('x')
    y = request.args.get('y')
    answer = modulo.modulo(x, y)
    output.answer = answer
    output.string = x + '%' + y + '=' + answer
    return jsonify(output)


if __name__ == '__main__':
    app.run()
# payload = {output}
# r = requests.post(url='/', json=payload)
# data = resp.json()
# print (data)
