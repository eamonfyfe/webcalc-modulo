from modulo import modulo
import unittest


class TestModulo(unittest.TestCase):

    def test_mod(self):
        res = modulo(10, 2)
        self.assertEqual(res, 0)


if __name__ == '__main__':
    unittest.main()
